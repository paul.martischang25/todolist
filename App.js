import 'react-native-gesture-handler';
import React from 'react';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer} from "@react-navigation/native";
import { Dimensions } from "react-native-web";
import { Feather } from "@expo/vector-icons";
import { TodoListsDone, TodoListsInProgress, TodoLists, AddTodoList } from "./screens";

// const DrawerNavigator = createDrawerNavigator({
//     "My Todolists": TodoLists,
//     "Add Todolist": AddTodoList,
//     "Todolists in progress": TodoListsInProgress,
//     "Todolists done": TodoListsDone
// });
//
// export default createAppContainer(DrawerNavigator);

const Drawer = createDrawerNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="MyTodolists"
                screenOptions= {{
                    swipeEnabled: true,
                    gestureEnabled: true,
                    headerShown: true
                }}
            >
                <Drawer.Screen name="My TodoLists" component={TodoLists} />
                <Drawer.Screen name="Add Todolist" component={AddTodoList} />
                <Drawer.Screen name="Todolists in progress" component={TodoListsInProgress} />
                <Drawer.Screen name="Todolists done" component={TodoListsDone} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}