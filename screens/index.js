import React from 'react';
import MyTodoLists from "./MyTodoLists";
import AjouterTodoList from "./AddTodoList";
import ThirdType from "./TodoListsDone";
import SecondType from "./TodoListsInProgress";

export const TodoLists = ({ navigation }) => <MyTodoLists navigation = {navigation} name="MyTodoLists" />;
export const AddTodoList = ({ navigation }) => <AjouterTodoList navigation = {navigation} name="AddTodoList" />;
export const TodoListsInProgress = ({ navigation }) => <SecondType navigation = {navigation} name="TodoListsInProgress" />;
export const TodoListsDone = ({ navigation }) => <ThirdType navigation = {navigation} name="TodoListsDone" />;


