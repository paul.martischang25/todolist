import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, Touchable } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import {TouchableOpacity} from "react-native-web";
import { AntDesign } from '@expo/vector-icons';

export default class TodoListsDone extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: "row" }}>
                    <View styles={styles.divider} />
                    <Text style={styles.title}>
                        Todo <Text style={{ fontWeight: "300", color: "#24A6D9" }}>Lists Done</Text>
                    </Text>
                </View>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    divider: {
        backgroundColor: "#A7CBD9",
        height: 1,
        flex: 1,
        alignSelf: "center"
    },
    title: {
        fontSize: 34,
        fontWeight: "300",
        color: "#2D3436",
        paddingHorizontal: 64
    }
});