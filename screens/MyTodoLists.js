import React, {useEffect, useState } from 'react';
import { View, Text, StyleSheet, Modal, Button } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AntDesign, FontAwesome5} from "@expo/vector-icons";
import {TouchableOpacity} from "react-native-web";
import { useFocusEffect } from '@react-navigation/native';
import buttonLikeRoles from "react-native-web/dist/modules/AccessibilityUtil/buttonLikeRoles";

const MyTodoLists = ({navigation}) => {
    const [ todoListName, setTodoList ] = useState([]);
    const getTodoLists = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('@storage_Key');
            const todolist = jsonValue != null ? JSON.parse(jsonValue) : [];
            console.log(todolist);
            setTodoList(todolist);

        } catch(e) {
            alert('Failed to get the data from the storage : [' + e + ']');
        }
        console.log('Done.')
    };
    const dropTodoLists = async() => {
        AsyncStorage.removeItem('@storage_Key');
        alert('Liste vide, merci de recharger la page !')
    };
    useFocusEffect(
        React.useCallback(() => {
            getTodoLists();
        }, [])
    );

    const removeActualList = async(index) => {
        const indexToRemove = index - 1;
        console.log( indexToRemove );
        await AsyncStorage.removeItem(indexToRemove);
        console.log("TodoList " + indexToRemove + " deleted.");
    };

    const getDescription = async(index) => {
        const jsonValue = await AsyncStorage.getItem('@storage_Key');
        const todolist = jsonValue != null ? JSON.parse(jsonValue) : [];
        const description = todolist[index+1];
        alert('Description de la todoList \'' + todolist[index] + '\' : ' + description);
    };

    return (
        <View style={styles.container}>

            <View style={{ flexDirection: "row" }}>
                <Text style={styles.title}>
                    <Text style={{ fontWeight: "300", color: "#24A6D9" }}>My Todo</Text> Lists
                </Text>
            </View>
            <View>
                <Text style={{fontSize: 15, fontWeight: '300', marginBottom: 10}}>Press on one TodoList button to get the description of the task !</Text>
            </View>
            <View>
                {todoListName.map((todo, index) => (
                    !(index % 2)
                    ?<TouchableOpacity onPress={() => getDescription(index)} style={styles.todoDesc} key={`${todo}-${index}`}>{todo}</TouchableOpacity>
                    :<TouchableOpacity style={styles.removeTask} onPress={() => removeActualList(index)}>
                            <AntDesign name="delete" size={20} color="#24A6D9" />
                        </TouchableOpacity>
                ) )}

            </View>
            <View style={{ marginVertical: 20 }}>
                <TouchableOpacity style={styles.addList} onPress={() => dropTodoLists()}>
                    <AntDesign name="poweroff" size={25} color="#24A6D9" />
                </TouchableOpacity>
                <Text style={styles.add}>Remove All</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    divider: {
        backgroundColor: "#A7CBD9",
        height: 1,
        flex: 1,
        alignSelf: "center"
    },
    title: {
        fontSize: 34,
        fontWeight: "300",
        color: "#2D3436",
        paddingHorizontal: 64,
        marginBottom: 20
    },
    addList: {
        borderWidth: 2,
        borderColor: '#A7CBD9',
        borderRadius: 4,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    add: {
        color: '#24A6D9',
        fontWeight: "600",
        fontSize: 14,
        marginTop: 8
    },
    todoContainer: {
        borderWidth: 2,
        borderColor: '#A7CBD9',
        borderRadius: 4,
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bloc: {
        borderWidth: 2,
        borderColor: '#000',
        borderRadius: 4,
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FF0000'
    },
    todoDesc: {
        color: '#FFF',
        backgroundColor: '#C5CDD2',
        fontSize: 18,
        padding: 10,
        textAlign: 'center',
        borderRadius: 5
    },
    removeTask: {
        borderWidth: 2,
        borderColor: '#A7CBD9',
        borderRadius: 4,
        padding: 5,
        alignItems: 'center',
        marginBottom: 10
    }
});

export default MyTodoLists;