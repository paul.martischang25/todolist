import React, {useState} from 'react';
import { View, Text, StyleSheet, Modal, Button, TextInput } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AntDesign, FontAwesome5} from "@expo/vector-icons";
import {TouchableOpacity} from "react-native-web";

const AjouterTodoList = () => {

    const [value, onChangeText] = React.useState('');
    const [description, onChangeDescription] = React.useState('');

    const storeData = async (value, description) => {
        try {
            const jsonValue = await AsyncStorage.getItem('@storage_Key');
            const todoListArray = jsonValue != null ? JSON.parse(jsonValue) : [];
            todoListArray.push(value, description);
            await AsyncStorage.setItem('@storage_Key', JSON.stringify(todoListArray));
            alert('TodoList Saved !');
        } catch (e) {
            alert('Error saving data' + e);
        }
    };

    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
                <View styles={styles.divider} />
                <Text style={styles.title}>
                    <Text style={{ fontWeight: "300", color: "#24A6D9" }}>Add Todo</Text> List
                </Text>
            </View>
            <TextInput
                style={{ marginTop: 16, marginBottom: 5, color:"#A7CBD9", height: 30, borderColor: '#24A6D9', width: 240, textAlign: 'center', borderWidth: 1 }}
                onChangeText={text => onChangeText(text)}
                value={value}
                placeholder='TodoList name'
            />
            <TextInput
                style={{ marginTop: 5, marginBottom: 10, color:"#A7CBD9", height: 30, borderColor: '#24A6D9', width: 240, textAlign: 'center', borderWidth: 1 }}
                onChangeText={text => onChangeDescription(text)}
                value={description}
                placeholder='Set a description'
            />
            <Button
                title="Add TodoList"
                color="#A7CBD9"
                onPress={() => storeData(value, description)}
            />
        </View>

    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    divider: {
        backgroundColor: "#A7CBD9",
        height: 1,
        flex: 1,
        alignSelf: "center"
    },
    title: {
        fontSize: 30,
        fontWeight: "300",
        color: "#2D3436",
        paddingHorizontal: 64
    },
    addList: {
        borderWidth: 2,
        borderColor: '#A7CBD9',
        borderRadius: 4,
        padding: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    add: {
        color: '#24A6D9',
        fontWeight: "600",
        fontSize: 14,
        marginTop: 8
    },
    mainContainer: {
        marginTop: 22,
    },
    modalContainer: {
        marginTop: 22,
    }
});

export default AjouterTodoList;
